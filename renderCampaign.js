"use strict"

function loadJqueryScript(src) {
	return new Promise((resolve, reject) => {
		var script;
		script = document.createElement('script');
		script.async = true;
		script.src = src;
		script.onload = resolve;
		script.onerror = reject;
		document.head.appendChild(script);
	});
}

async function fetchJqueryScript() {
	if (!window.jQuery) {
		await loadJqueryScript(`https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js`);
	}
}



function createHtmlTagsWithTimeStamp(timeStamp, baseUrl, templatePlaceholder) {
	let paramString = '';
	if (timeStamp !== undefined)
		paramString = '?cacheId=' + timeStamp;

	const styleTag = document.createElement('link');
	styleTag.rel = 'stylesheet';
	styleTag.href = baseUrl + 'style.min.css' + paramString;

	const scriptTag = document.createElement('script');
	scriptTag.src = baseUrl + 'script.min.js' + paramString;

	fetch(baseUrl + 'template.html' + paramString).then((response) => {
		return response.text();
	}).then((data) => {
		templatePlaceholder.innerHTML = data;
		templatePlaceholder.appendChild(styleTag);
		templatePlaceholder.appendChild(scriptTag);
	})
}

export function renderTemplate (templatePlaceholder, campaignId, domain) {
	Promise.all([
			fetchJqueryScript(),
			fetch(domain + '/' + campaignId + '/templateMeta.json' + '?cacheBuster=' + Math.floor(Math.random() * 1000000))
		])
		.then((responses) => { return responses[1].json() })
		.then((data) => { createHtmlTagsWithTimeStamp(data.timePublished, domain + '/' + campaignId + '/', templatePlaceholder);})
}

// TRACKING MIGRATION BLOCK
// in renderer the analytics object is accessible with window.dmn.analytics
window.DMN = {};
DMN.analytics = {
	trackElementsWithDataLayerRefresh: function (value, paramName, singleValue) {
		// yes, use window.dmn
		window.dmn?.analytics.trackElements(value, paramName, singleValue);
	},

	trackEventWithDataLayerRefresh: function (paramName) {
		// yes, use window.dmn
		window.dmn?.analytics.trackEvents(paramName);
	},

	trackElements: function (value, paramName, singleValue) {
		// yes, use window.dmn
		window.dmn?.analytics.trackElements(value, paramName, singleValue);
	}
}

window.DMX_MANDATOR = centralData.mandator;
// END TRACKING MIGRATION BLOCK
